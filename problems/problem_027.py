# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

list = [10,1,2,3,4]
def max_in_list(values):
    if len(values) == 0:
        return None
    else:
        return max(values)

var = max_in_list(list)
print(var)
