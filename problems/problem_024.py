# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

list = [5,5,5,5,5]

def calculate_average(values):
    total = sum(values)
    average = total//len(values)

    if len(values) == 0:
        return None
    else:
        return average

var = calculate_average(list)
print(var)
