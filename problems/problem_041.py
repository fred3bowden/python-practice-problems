# Complete the add_csv_lines function which accepts a list
# as its only parameter. Each item in the list is a
# comma-separated string of numbers. The function should
# return a new list with each entry being the corresponding
# sum of the numbers in the comma-separated string.
#
# These kinds of strings are called CSV strings, or comma-
# sepearted values strings.
#
# Examples:
#   * input:  []
#     output: []
#   * input:  ["3", "1,9"]
#     output: [3, 10]
#   * input:  ["8,1,7", "10,10,10", "1,2,3"]
#     output:  [16, 30, 6]
#
# Look up the string split function to find out how to
# split a string into pieces.

# Write out your own pseudocode to help guide you.
def add_csv_lines(csv_lines):
   new_list = []
   newest_list = []
   for i in csv_lines:
      i = list(i) #breaks down every elemnt in a string into its own string
      list1 = []  #empty for now
      for j in i:
         if j != ',': #removes the commas
            list1.append(j) #just numbers

      new_list.append(list1)

   for k in new_list:
      count = 0
      for str in k:
         str = int(str)
         count += str

      newest_list.append(count)

   return newest_list


csv_lines = ["8,1,7", "20,20,20", "1,2,3"]
print(add_csv_lines(csv_lines))




   #return new_list
