# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    final_str = ""
    for letter in s:
        if len(s) == 0:
            return ""
        elif letter not in final_str:
                final_str += letter

    return final_str

var1 = remove_duplicate_letters("FredFred")
var2 = remove_duplicate_letters("abccba")
var3 = remove_duplicate_letters("abccbad")
print(var1, var2, var3)
