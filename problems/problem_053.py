# Write a function that meets these requirements.
#
# Name:       username_from_email
# Parameters: a valid email address as a string
# Returns:    the username portion of the email address
#
# The username portion of an email is the substring
# of the email address that appears before the @
#
# Examples
#    * input:   "basia@yahoo.com"
#      returns: "basia"
#    * input:   "basia.farid@yahoo.com"
#      returns: "basia.farid"
#    * input:   "basia_farid+test@yahoo.com"
#      returns: "basia_farid+test"

def username_from_email(email_string):
    return email_string.split('@')[0]
    # split() can split the string at the index of what you put in the ()
    # the split() creates a list of all of the strings where you split
    # in this case it only creates two elements indexed [0,1]
    # we use the [0] to get the username which is the first item

print(username_from_email('fred@gmail.com'))
