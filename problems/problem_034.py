# Complete the count_letters_and_digits function which
# accepts a parameter s that contains a string and returns
# two values, the number of letters in the string and the
# number of digits in the string
#
# Examples:
#   * "" returns 0, 0
#   * "a" returns 1, 0
#   * "1" returns 0, 1
#   * "1a" returns 1, 1
#
# To test if a character c is a digit, you can use the
# c.isdigit() method to return True of False
#
# To test if a character c is a letter, you can use the
# c.isalpha() method to return True of False
#
# Remember that functions can return more than one value
# in Python. You just use a comma with the return, like
# this:
#      return value1, value2


def count_letters_and_digits(s):
    total_letters = 0
    total_nums = 0
    for c in s:
        if c.isdigit() == True:
            total_nums += 1
        if c.isalpha() == True:
            total_letters += 1
    return total_letters, total_nums


var = count_letters_and_digits("1a")
var2 = count_letters_and_digits("")
var3 = count_letters_and_digits("1")
var4 = count_letters_and_digits("a")
print(var, var2, var3, var4)
