# Write a class that meets these requirements.
#
# Name:       Book
#
# Required state:
#    * author name, a string
#    * title, a string
#
# Behavior:
#    * get_author: should return "Author: «author name»"
#    * get_title:  should return "Title: «title»"
#
# Example:
#    book = Book("Natalie Zina Walschots", "Hench")
#
#    print(book.get_author())  # prints "Author: Natalie Zina Walschots"
#    print(book.get_title())   # prints "Title: Hench"
#
# There is pseudocode availabe for you to guide you


class Book:
    def __init__(self, author, title):
        self.author = author
        self.title = title
    # method initializer method with required state
    # parameters author and title
        # set self.author = author
        # set self.title = title


    # method
    def get_author(self):
        return self.author
        # returns "Author: " + self.author

    # method
    def get_title(self):
        return self.title
        # returns "Title: " + self.title

book1 = Book("Fred", "Fred's Book")
print(book1.get_author())
print(book1.get_title)
