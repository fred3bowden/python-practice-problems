# Write four classes that meet these requirements.
#
# Name:       Animal
class Animal:
    def __init__(self, legs, color):
        self.legs = legs
        self.color = color

    def describe(self):
        return (f'{self.__class__.__name__} has {self.legs} legs and is primarily {self.color}.')


#
# Required state:
#    * number_of_legs, the number of legs the animal has
#    * primary_color, the primary color of the animal
#
# Behavior:
#    * describe()       # Returns a string that describes that animal
#                         in the format
#                                self.__class__.__name__
#                                + " has "
#                                + str(self.number_of_legs)
#                                + " legs and is primarily "
#                                + self.primary_color
#
#
class Dog(Animal):
    def __init__(self, legs, color):
        super().__init__(legs, color)

    def behavior(self):
        return "Bark!"

class Cat(Animal):
    def __init__(self, legs, color):
        super().__init__(legs, color)

    def behavior(self):
        return "Meow!"

class Snake(Animal):
    def __init__(self, legs, color):
        super().__init__(legs, color)

    def behavior(self):
        return "Ssss!"


dog = Dog(4, "Black")
print(dog.describe())


# Name:       Dog, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Bark!"
#
#
#
# Name:       Cat, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Miao!"
#
#
#
# Name:       Snake, inherits from Animal
#
# Required state:       inherited from Animal
#
# Behavior:
#    * speak()          # Returns the string "Sssssss!"
